package sheridan;

import java.math.BigInteger;
import java.util.Random;
import java.util.logging.Logger;

public class PrimeNumber {

	/**
 * @param args
 */
public static void main(String[] args) {
	printTest(10, 4);
	printTest(2, 2);
	printTest(54161329, 4);
	printTest(1882341361, 2);
	printTest(36, 9);

	System.out.println(isPrime(54161329) + " expect false");
	System.out.println(isPrime(1882341361) + " expect true");
	System.out.println(isPrime(2) + " expect true");
	
	int numPrimes = 0;
	for(int i = 2; i < 10000000; i++) {
		if(isPrime(i)) {
			numPrimes++;
		}
	}
	boolean[] primes = getPrimes(10000000);
	int np = 0;
	for(boolean b : primes)
	{
		if(b)
		{
			np++;
		}
	}


	System.out.println(new BigInteger(1024, 10, new Random()));
}

public static boolean[] getPrimes(int max) {
	boolean[] result = new boolean[max + 1];
	for(int i = 2; i < result.length; i++)
	{
		result[i] = true;
	}
	final double LIMIT = Math.sqrt(max);
	for(int i = 2; i <= LIMIT; i++) {
		if(result[i]) {
			int index = 2 * i;
			while(index < result.length){
				result[index] = false;
				 index += i;
			}
		}
	}
	return result;
}


public static void printTest(int num, int expectedFactors) {

	int actualFactors = numFactors(num);

	System.out.println("Testing " + num + " expect " + expectedFactors + ", " +
			"actual " + actualFactors);
	if(actualFactors == expectedFactors)
	{
		System.out.println("PASSED");
	}
	    else
	    {
		System.out.println("FAILED");
	    }
}

public static boolean isPrime(int num) {
	if( num >= 2)
		{
		System.out.println("failed precondition. num must be >= 2. num: " + num);
		}
	final double LIMIT = Math.sqrt(num);
	boolean isPrime = (num == 2) ? true : num % 2 != 0;
	int div = 3;
	while(div <= LIMIT && isPrime) {
		isPrime = num % div != 0;
		div += 2;
	}
	return isPrime;
}

// pre: num >= 2
public static int numFactors(int num) {
	if( num >= 2)
	{
		System.out.println( "failed precondition. num must be >= 2. num: " + num);
	}
	int result = 0;
	final double SQRT = Math.sqrt(num);
	for(int i = 1; i < SQRT; i++) {
		if(num % i == 0) {
			result += 2;
		}
	}
	if(num % SQRT == 0)
	{
		result++;
	}
	    return result;
}

}